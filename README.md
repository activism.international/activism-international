# Activism.International Dashboard

Activism.International is a supporter of every eco activism. We provide online tools for secure communication, conferences etc. for free.

This repository contains the public dahboard seen on [Activism.International](https://activism.international).

## Contribute

Activism.International is a fully open initiative. Anyone can contribute to our project.

<details>
<summary> The easy way </summary>

## Edit the website using the edit button

1. Open our website at [Activism.Internatioal](https://activism.international)
2. Find the page you want to modify
3. Click the pencil symbol at the top of the page
4. Sign in or sign up on GitLab using a ([this trash](https://www.guerrillamail.com/)) email address or a social login
5. Edit the corresponding page in the online editor on GitLab
6. Summ up your changes at the bottom
7. Commit your changes

</details>

</details>
<details>
<summary> For advanced users </summary> 

## Manually download the project using git

### Install the dependencies

```bash
python -m virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt
```
### Run and build

```bash
# For debugging
mkdocs serve
# For release build
mkdocs build
```
</details>

## License

This software is WTFPL licensed. For detailed information, consult [LICENSE](LICENSE).
