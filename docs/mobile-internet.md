# Mobile internet

In some countries, we can provide mobile internet for direct actions, events or climate camps.

We provide two different kinds of internet access points:

- Fast and efficient: We feature **unlimited data** at modern speeds between **50 Mbit/s up to 250 Mbit/s**.
- Fully encrypted and secure: **All traffic is being encrypted** using [TOR](https://torproject.org) and made **unidentifiable** hence but at much, **much lower speed** of about 2 Mbit/s - 10 Mbit/s.

Even though most of our services are completely for free, we kindly ask for donations in case you use our mobile internet at an event.

You can estimate an amount for a donation by about **EUR 10 - EUR 25 for providing equipment** plus about **EUR 1.50 - EUR 2.50 per day**.

Of course, the price for mobile internet is solidary. In case this is too much for your camp, event or group, just get in touch and we can find a solution.

Please contact us by [mail](mailto:info@activism.international) [[PGP Key](https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xd89f1815be63afa963f2641be34177cd2dd58da0)] to check whether you can use our mobile internet for your event.